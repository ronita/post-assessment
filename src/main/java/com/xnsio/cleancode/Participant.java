package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.util.List;

public class Participant {
    private String name;
    private List<LocalDateTime> calendar;

    public Participant(String name, List<LocalDateTime> calendar) {
        this.name = name;
        this.calendar = calendar;
    }
//
    public String getName() {
        return name;
    }

    public List<LocalDateTime> getCalendar() {
        return calendar;
    }
}
