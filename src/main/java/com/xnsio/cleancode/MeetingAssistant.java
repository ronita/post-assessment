package com.xnsio.cleancode;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MeetingAssistant {

    private static final Integer MEETING_START_HOUR = 8;
    private static final Integer MEETING_END_HOUR = 17;

    public String setupMeetingFor(final List<Participant> participantCalendars,final LocalDateTime startDateTime, final LocalDateTime endDateTime){

        List<LocalDateTime> allSlots = getAllSlots(startDateTime, endDateTime);

        for(LocalDateTime slot : allSlots) {
            boolean noneMatch = participantCalendars.stream().noneMatch(participant -> participant.getCalendar().stream().anyMatch(date -> date.compareTo(slot) ==0));
            if(noneMatch){
                return slot.toString();
            }
        }

        return "No common meeting time available";
    }

    private List<LocalDateTime> getAllSlots(LocalDateTime startDateTime, LocalDateTime endDateTime) {
        LocalDateTime dateTime;
        List< LocalDateTime > slots = new ArrayList<>() ;
        if(startDateTime != null){
            dateTime = startDateTime.withNano(0).withMinute(0).withSecond(0);
        }else {
            dateTime = LocalDateTime.now().plusHours(1).withNano(0).withMinute(0).withSecond(0);
        }

        while(dateTime.isBefore(endDateTime)) {
            if(dateTime.getHour() >= MEETING_START_HOUR && dateTime.getHour() < MEETING_END_HOUR) {
                slots.add(dateTime);
            }
            // Prepare for next loop.
            dateTime = dateTime.plusHours( 1 ) ;
        }
        return slots;
    }
}