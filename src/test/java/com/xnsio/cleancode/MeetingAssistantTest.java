package com.xnsio.cleancode;

import org.junit.Test;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class MeetingAssistantTest {

    private final String DATE_TIME_FORMAT_WITHOUT_SECONDS = "yyyy-MM-dd HH:mm";

    @Test
    public void setupMeeting() {
        //given
        List<Participant> participants = new ArrayList<>();
        participants.add(setupParticipantCalender("Mini", buildMeetingCalendar("2019-11-14 10:00", "2019-11-14 11:00", "2019-11-14 12:00", "2019-11-14 13:00", "2019-11-14 15:00")));
        participants.add(setupParticipantCalender("Brad", buildMeetingCalendar("2019-11-14 10:00", "2019-11-14 11:00", "2019-11-14 13:00", "2019-11-14 15:00")));
        //when
        MeetingAssistant meetingAssistant = new MeetingAssistant();
        String meetingTime = meetingAssistant.setupMeetingFor(participants, getLocalDateTime("2019-11-14 10:00"), getLocalDateTime("2019-11-14 17:00"));
        //thenn
        assertEquals("2019-11-14T14:00", meetingTime);
    }

    @Test
    public void meetingNotPossible() {
        //given
        List<Participant> participants = new ArrayList<>();
        participants.add(setupParticipantCalender("Mini", buildMeetingCalendar("2019-11-14 10:00", "2019-11-14 11:00", "2019-11-14 12:00", "2019-11-14 13:00", "2019-11-14 15:00")));
        participants.add(setupParticipantCalender("Brad", buildMeetingCalendar("2019-11-14 10:00", "2019-11-14 11:00", "2019-11-14 13:00", "2019-11-14 15:00")));
        //when
        MeetingAssistant meetingAssistant = new MeetingAssistant();
        String meetingTime = meetingAssistant.setupMeetingFor(participants, getLocalDateTime("2019-11-14 10:00"), getLocalDateTime("2019-11-14 12:00"));
        //then
        assertEquals("No common meeting time available", meetingTime);
    }

    @Test
    public void meetingPossible() {
        //given
        List<Participant> participants = new ArrayList<>();
        participants.add(setupParticipantCalender("Mini", buildMeetingCalendar("2019-11-14 10:00", "2019-11-14 11:00", "2019-11-14 12:00", "2019-11-14 13:00", "2019-11-14 14:00")));
        participants.add(setupParticipantCalender("Brad", buildMeetingCalendar("2019-11-14 10:00", "2019-11-14 11:00", "2019-11-14 13:00", "2019-11-14 14:00")));
        //when
        MeetingAssistant meetingAssistant = new MeetingAssistant();
        String meetingTime = meetingAssistant.setupMeetingFor(participants, getLocalDateTime("2019-11-14 10:00"), getLocalDateTime("2019-11-14 17:00"));
        //then
        assertEquals("2019-11-14T15:00", meetingTime);
    }

    @Test
    public void meetingPossibleNextDay() {
        //given
        List<Participant> participants = new ArrayList<>();
        participants.add(setupParticipantCalender("Mini", buildMeetingCalendar("2019-11-14 10:00", "2019-11-14 11:00", "2019-11-14 12:00", "2019-11-14 13:00", "2019-11-14 14:00",
                "2019-11-14 15:00", "2019-11-14 16:00", "2019-11-14 17:00")));
        participants.add(setupParticipantCalender("Brad", buildMeetingCalendar("2019-11-14 10:00", "2019-11-14 11:00", "2019-11-14 13:00", "2019-11-14 14:00")));
        //when
        MeetingAssistant meetingAssistant = new MeetingAssistant();
        String meetingTime = meetingAssistant.setupMeetingFor(participants, getLocalDateTime("2019-11-14 10:00"), getLocalDateTime("2019-11-15 14:00"));
        //then
        assertEquals("2019-11-15T08:00", meetingTime);
    }

    // Helper methods for test data
    private Participant setupParticipantCalender(String participantName, List<LocalDateTime> participantCalender) {
        return new Participant(participantName, participantCalender);
    }

    private List<LocalDateTime> buildMeetingCalendar(String... meetings) {
        List<LocalDateTime> calendar = new ArrayList<>();
        for (String meeting : meetings) {
            calendar.add(getLocalDateTime(meeting));
        }
        return calendar;
    }

    private LocalDateTime getLocalDateTime(String meeting) {
        return LocalDateTime.parse(meeting, DateTimeFormatter.ofPattern(DATE_TIME_FORMAT_WITHOUT_SECONDS));
    }
}